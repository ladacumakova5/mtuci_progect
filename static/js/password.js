function onChange() {
    const password = document.querySelector('input[id=new_password]');
    const repeat_password = document.querySelector('input[id=repeat_password]');
    if (repeat_password.value === password.value) {
      repeat_password.setCustomValidity('');
    } else {
      repeat_password.setCustomValidity('Пароль не совпадает');
    }
  }
