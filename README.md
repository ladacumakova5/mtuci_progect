# mtuci_progect

# Создание одинаковой бд
```sql
    create database mtuci_project;
    create user admins with password 'password';
    grant all privileges on database mtuci_project to admins;
```
После того как мы создали базу данных, пользователя и наградили его всеми мозвожными привилегиями осталось только создать все таблички.
Для этого в терминале пропишим следующее:
```shell
   python manage.py makemigration
   python manage.py migrate
```

# Страница входа
В скобках обозначается то что принемает front это эже отдается на back

- Контактный телефон ("telephone")
- Пароль ("password'')

# Страница регистрации
В скобках обозначается то что принемает front это эже отдается на back.\
Все условия (проверки) должны быть прописаны на фронте

- Имя ("name")
- Фамилия ("surname")
- Контактный телефон ("telephone")
- Почта ("email")
  - Условие на почту: должен присутствовать '@' и после него '.'  *(@gmail.com)*
- Пароль ("password")
  - Условие на пароль: должен содержать только латинские буквы, цифры и знаки: @,?,_,!
- Повторите пароль ("repeat_password")
  - Условие на повторный пароль: должен совпадать с password

# Таблицы в базе данных

## Users
- id: *serial*
- name: *varchar(50)*
- surname: *varchar(50)*
- telephone: *varchar(12)*
- email: *varchar(100)*
- password: *varchar(100)*
- address: *varchar(100)*
