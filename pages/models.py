from django.contrib.auth.backends import UserModel
from django.contrib.auth.models import User

from django.db import models


# Create your models here.

class Users(models.Model):
    username = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    telephone = models.CharField(max_length=12)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=100)
    address = models.CharField(max_length=100, blank=True)
    isActive = models.BooleanField(default=False)
    object = models.Manager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=('telephone', 'email'),
                name='first_and_last_names_different'),
        ]


class Token(models.Model):
    token = models.CharField(max_length=1000)
    user = models.OneToOneField(Users, on_delete=models.CASCADE, primary_key=True)
    object = models.Manager()


# class Users(User):
#     name = models.CharField(max_length=50)
#     surname = models.CharField(max_length=50)
#     telephone = models.CharField(max_length=12)
#     email = models.EmailField(max_length=100)
#     password = models.CharField(max_length=100)
#     address = models.CharField(max_length=100, blank=True)
#     object = models.Manager()
#
#     class Meta:
#         constraints = [
#             models.UniqueConstraint(
#                 fields=('telephone', 'email'),
#                 name='first_and_last_names_different'),
#         ]
