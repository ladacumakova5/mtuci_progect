import jwt
from rest_framework.serializers import ModelSerializer

from pages.models import Users, Token


class UserSerializer(ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'

    def create(self, validated_data):
        user = Users(
            email=validated_data['email'],
            username=validated_data['username'],
            password=validated_data['password'],
            surname=validated_data['surname'],
            telephone=validated_data['telephone']
        )
        print(user.username)
        if "address" in validated_data.keys():
            user.address = validated_data["address"]

        user.isActive = True

        user.save()
        print(20000000)
        token_n = jwt.encode({"some": user.email}, "secret", algorithm="HS256")
        token = Token(
            token=token_n,
            user=user
        )
        print(300000000)
        token.save()
        return token

    @staticmethod
    def find(data):
        try:
            user = Users.object.get(telephone=data['telephone'])
            user.isActive = True
            token_n = jwt.encode({"some": user.email}, "secret", algorithm="HS256")
            token = Token(
                token=token_n,
                user=user
            )
            token.save()
        except Exception:
            raise Exception('Такого пользователя нет')

        if data['password'] != user.password:
            raise Exception('Неверный пароль')
        return token

    def update(self, instance, validated_data):
        if "password" in validated_data.keys():
            instance.password = validated_data['password']
        if "email" in validated_data.keys():
            instance.email = validated_data['email']
        if "username" in validated_data.keys():
            instance.username = validated_data['username']
        if "surname" in validated_data.keys():
            instance.surname = validated_data['surname']
        if "telephone" in validated_data.keys():
            instance.telephone = validated_data['telephone']
        if "address" in validated_data.keys():
            instance.address = validated_data['address']
        instance.save()
        return instance


class TokenSerializer(ModelSerializer):
    class Meta:
        model = Token
        fields = ['token']

    def find(self, data):
        try:
            token = Token.object.get(token=data['token'])
        except:
            raise Exception("Не возможно найти пользователя")
        return token.user
