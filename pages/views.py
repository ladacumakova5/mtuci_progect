# from django.shortcuts import render
# from django.http import HttpResponse
#
#
# def homePageView(request):
#     return HttpResponse("Hello, World!")
import jwt
from django.template.response import TemplateResponse
from django.views.generic import TemplateView
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.views import APIView

from .models import Users, Token
from .serializer import UserSerializer, TokenSerializer


# class HomePageView(TemplateView):
#     template_name = "home.html"
#
#
# class AboutPageView(TemplateView):
#     template_name = "about.html"


class LoginView(APIView):
    def get(self, request):
        return render(request, "index.html")

    def post(self, request):
        data = request.data
        if "signup" in data.keys():
            ser = UserSerializer(data=data)
            ser.is_valid(raise_exception=True)
            try:
                token = ser.create(data)
                # encoded_jwt = jwt.encode({"some": u.username}, "secret", algorithm="HS256")
                # print(encoded_jwt)
                # token = Token.object.create(user=u)
                # return HttpResponseRedirect("/main/")
            except Exception:
                return Response({"exception": "Такой пользователь уже есть"})
        elif "login" in data.keys():
            try:
                token = UserSerializer.find(data)
                # return HttpResponseRedirect("main/")
            except Exception as error:
                return Response({"exception": error.args[0]})
        return HttpResponseRedirect("/main/?token=" + token.token)


class MainView(APIView):
    def get(self, request):
        token = request.GET.get("token")
        return render(request, "main_page.html", {"token": token})

    def post(self, request):
        data = request.data
        if "lk" in data.keys():
            return HttpResponseRedirect("/user/?token=" + request.GET.get("token"))


class UserLKView(APIView):

    def get(self, request):
        data = request.data
        try:
            tk = Token.object.get(token=data['token'])
            # user = tk.user
            # response_data = {
            #     'username': user.username,
            #     'surname': user.surname,
            #     'telephone': user.telephone,
            #     'email': user.email,
            #     'address': user.address,
            #     'token': data['token']
            # }
        except:
            tok = request.GET.get("token")
            tk = Token.object.get(token=tok)
        user = tk.user
        response_data = {
            'username': user.username,
            'surname': user.surname,
            'telephone': user.telephone,
            'email': user.email,
            'address': user.address,
            'token': tk.token
        }
        return render(request, "cabinet.html", response_data)

    def post(self, request):
        data = request.data
        if "settings" in data.keys():
            return HttpResponseRedirect("/user/settings/?token=" + request.GET.get("token"))
        if "change/password" in data.keys():
            return HttpResponseRedirect("/user/change/password/?token=" + request.GET.get("token"))


class UserSettings(APIView):
    def get(self, request):
        data = request.data
        try:
            tk = Token.object.get(token=data['token'])
            # user = tk.user
            # response_data = {
            #     'username': user.username,
            #     'surname': user.surname,
            #     'telephone': user.telephone,
            #     'email': user.email,
            #     'address': user.address,
            #     'token': data['token']
            # }
        except:
            tok = request.GET.get("token")
            tk = Token.object.get(token=tok)
        user = tk.user
        response_data = {
            'username': user.username,
            'surname': user.surname,
            'telephone': user.telephone,
            'email': user.email,
            'address': user.address,
            'token': tk.token
        }
        return render(request, "settings.html", response_data)

    def put(self, request):

        data = request.data
        try:
            tk = Token.object.get(token=data['token'])

        except:
            tok = request.GET.get("token")
            tk = Token.object.get(token=tok)
        user = tk.user

        upduser = UserSerializer.update(self, user, data)
        response_data = {
            'username': upduser.username,
            'surname': upduser.surname,
            'telephone': upduser.telephone,
            'email': upduser.email,
            'address': upduser.address,
            'token': tk.token
        }
        return HttpResponseRedirect("/user/?token="+tk.token)

    def post(self, request):
        data = request.data
        if "save" in data.keys():
            return UserSettings.put(self, request)
        elif "delete_account" in data.keys():
            return UserSettings.delete(self, request)
        else:
            return Response("HI")

    def delete(self, request):
        return Response("del")


class UserChangePassword(APIView):
    def get(self, request):
        return render(request, "password.html")

# class CustomAuthToken(ObtainAuthToken):
#
#     def post(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.data,
#                                            context={'request': request})
#         serializer.is_valid(raise_exception=True)
#         user = serializer.validated_data['user']
#         token, created = Token.objects.get_or_create(user=user)
#         return Response({
#             'token': token.key,
#             'user_id': user.pk,
#             'email': user.email
#         })
# @api_view(['GET', 'POST'])
# @csrf_exempt
# def login(request):
#     if request.method == 'POST':
#         data = request.data
#         if "signup" in data.keys():
#             ser = UserSerializer(data=data)
#             ser.is_valid(raise_exception=True)
#             try:
#                 ser.create(data)
#                 return HttpResponseRedirect("/about/")
#             except ValueError:
#                 return Response({"exception": "Такой пользователь уже есть"})
#         elif "login" in data.keys():
#             try:
#                 user = UserSerializer.find(data)
#                 return HttpResponseRedirect("/about/")
#             except Exception as error:
#                 return Response({"exception": error.args[0]})
#     else:
#         return render(request, "index.html")
