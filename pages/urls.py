from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from .views import LoginView, MainView, UserLKView, UserSettings, UserChangePassword

urlpatterns = [
                  # path("", HomePageView.as_view(), name="home"),
                  # path("about/", AboutPageView.as_view(), name="about"),
                  path("login/", LoginView.as_view()),
                  path("main/", MainView.as_view()),
                  path("user/", UserLKView.as_view()),
                  path("user/settings/", UserSettings.as_view()),
                  path("user/change/password/", UserChangePassword.as_view()),
                  # path('api-token-auth/', obtain_auth_token)
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
